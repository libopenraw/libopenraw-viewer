libopenraw-viewer
=================

libopenraw-viewer is an experimental application that is used for the
development of [libopenraw](https://libopenraw.freedesktop.org/), more
specifically for the Rust rewrite of libopenraw.

The application is written in Rust, using the relm4 framework, and
gtk4.

libopenraw-viewer is licensed under the GPL3 or later.

Maintainer
----------

Hubert Figuiere