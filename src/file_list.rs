// SPDX-License-Identifier: GPL-3.0-or-later

use std::io::{Read, Write};

use gtk4::prelude::*;
use relm4::gtk as gtk4;
use relm4::prelude::*;
use relm4::{factory::FactoryVecDeque, ComponentParts, ComponentSender, SimpleComponent};
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
struct Config {
    files: Vec<String>,
}

#[derive(Debug)]
pub(super) enum RawFileListInput {
    None,
    /// Add a file to the list
    Add(String),
    RemoveSelected,
    /// Item has been selected
    Selected(i32),
    /// Select item at
    Select(usize),
    /// Select next item
    Next,
    /// Select previous item
    Previous,
    /// Save to file
    SaveTo(std::path::PathBuf),
    /// Load the config to a file
    LoadConfig(std::path::PathBuf),
}

#[derive(Debug)]
pub(super) enum RawFileListOutput {
    SelectionChanged(String, usize),
    CountChanged(usize),
}

#[derive(Default)]
pub(super) struct Item {
    path: String,
    filename: String,
}

#[relm4::factory(pub(crate))]
impl FactoryComponent for Item {
    type Init = String;
    type Input = ();
    type Output = ();
    type CommandOutput = ();
    type Widgets = ListWidgets;
    type ParentWidget = gtk4::ListBox;

    fn init_model(path: Self::Init, _index: &DynamicIndex, _sender: FactorySender<Self>) -> Self {
        if let Some(file_name) = std::path::PathBuf::from(&path).file_name() {
            let filename = if let Some(dir) = std::path::PathBuf::from(&path)
                .parent()
                .and_then(std::path::Path::file_name)
            {
                format!("{}/{}", dir.to_string_lossy(), file_name.to_string_lossy())
            } else {
                format!("{}", file_name.to_string_lossy())
            };
            Self { path, filename }
        } else {
            Self {
                filename: path.clone(),
                path,
            }
        }
    }

    view! {
        root = gtk4::Label {
            #[watch]
            set_label: &self.filename,
            set_xalign: 0.0,
            set_ellipsize: gtk4::pango::EllipsizeMode::Start,
            set_width_chars: 15,
        }
    }
}

pub(crate) struct RawFileList {
    pub(crate) files: FactoryVecDeque<Item>,
    current_file: usize,
}

impl RawFileList {
    /// Add a file is supported and not alread in the list.
    /// Return `true` if added.
    fn add_file(&mut self, p: String) -> bool {
        if let Some(ext) = std::path::Path::new(&p).extension() {
            if libopenraw::extensions()
                .iter()
                .any(|v| v.as_str() == ext.to_ascii_lowercase())
            {
                let files = &mut self.files.guard();
                if !files.iter().any(|v| v.path == p) {
                    files.push_back(p);
                    return true;
                }
            }
        }
        false
    }
}

#[relm4::component(pub(crate))]
impl SimpleComponent for RawFileList {
    type Init = ();
    type Input = RawFileListInput;
    type Output = RawFileListOutput;

    fn init(
        _params: Self::Init,
        root: Self::Root,
        sender: ComponentSender<Self>,
    ) -> ComponentParts<Self> {
        let model = RawFileList {
            files: FactoryVecDeque::builder()
                .launch(gtk4::ListBox::default())
                .forward(sender.input_sender(), |_| Self::Input::None),
            current_file: 0,
        };
        let list_box = model.files.widget();
        let widgets = view_output!();

        ComponentParts { model, widgets }
    }

    fn update(&mut self, msg: Self::Input, sender: ComponentSender<Self>) {
        use RawFileListInput::*;
        match msg {
            None => {}
            Add(p) => {
                if self.add_file(p) {
                    let _ = sender.output(RawFileListOutput::CountChanged(self.files.len()));
                }
            }
            RemoveSelected => {
                self.files.guard().remove(self.current_file);
                let _ = sender.output(RawFileListOutput::CountChanged(self.files.len()));
                if self.current_file >= self.files.len() {
                    self.current_file = self.files.len() - 1;
                    sender.input(RawFileListInput::Select(self.current_file));
                }
            }
            Selected(idx) => {
                let idx = idx as usize;
                if let Some(file) = self.files.get(idx) {
                    let _ = sender.output(RawFileListOutput::SelectionChanged(
                        file.path.to_string(),
                        idx,
                    ));
                    self.current_file = idx;
                }
            }
            Select(idx) => {
                let list_box = self.files.widget();
                let item = list_box.row_at_index(idx as i32);
                list_box.select_row(item.as_ref());
                self.current_file = idx;
            }
            Next => {
                if !self.files.is_empty() && self.current_file < self.files.len() - 1 {
                    self.current_file += 1;
                    sender.input(RawFileListInput::Select(self.current_file));
                }
            }
            Previous => {
                if self.current_file > 0 {
                    self.current_file -= 1;
                    sender.input(RawFileListInput::Select(self.current_file));
                }
            }
            LoadConfig(p) => {
                if let Ok(config_str) = std::fs::File::open(p).and_then(|mut file| {
                    let mut config = String::default();
                    file.read_to_string(&mut config)?;
                    Ok(config)
                }) {
                    let config = toml::from_str::<Config>(&config_str);
                    if let Ok(config) = config {
                        for item in config.files {
                            self.files.guard().push_back(item);
                        }
                        let _ = sender.output(RawFileListOutput::CountChanged(self.files.len()));
                    }
                }
            }
            SaveTo(p) => {
                let config = Config {
                    files: self.files.iter().map(|i| i.path.clone()).collect(),
                };
                toml::to_string_pretty(&config)
                    .map(|save| {
                        std::fs::create_dir_all(p.parent().unwrap())
                            .expect("couldn't create the config dir");
                        std::fs::File::create(&p)
                            .and_then(|mut file| file.write(save.as_bytes()))
                            .expect("Failed to write config");
                    })
                    .expect("Failed to save config");
            }
        }
    }

    view! {
        #[root]
        gtk4::ScrolledWindow {
            set_vexpand: true,
            #[local_ref]
            list_box -> gtk4::ListBox {
                set_size_request: (100, -1),
                set_vexpand: true,
                //factory!(model.files),
                connect_row_selected[sender] => move |_, row| {
                    if let Some(row) = row {
                        let idx = row.index();
                        sender.input(RawFileListInput::Selected(idx));
                    }
                }
            }
        }
    }
}
