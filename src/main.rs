// SPDX-License-Identifier: GPL-3.0-or-later

mod file_list;
mod processing;
mod rawinfo;
mod utils;

use std::convert::identity;

use gtk::cairo;
use gtk::prelude::*;
use libopenraw::{
    Bitmap, ColourSpace, DataType, RawImage, RenderingOptions, RenderingStage,
};
use relm4::abstractions::{drawing, DrawHandler};
use relm4::adw;
use relm4::gtk;
use relm4::{
    Component, ComponentController, ComponentParts, ComponentSender, Controller, RelmApp,
    RelmWidgetExt, SimpleComponent,
};
use relm4_components::open_dialog::{
    OpenDialogMsg, OpenDialogMulti, OpenDialogResponse, OpenDialogSettings,
};

use file_list::{RawFileList, RawFileListInput, RawFileListOutput};
use rawinfo::{RawInfo, RawInfoMsg};
use utils::files_from_directory;

fn config_path() -> Option<std::path::PathBuf> {
    dirs_next::config_dir().map(|p| p.join("org.freedesktop.libopenraw.Viewer/viewer-config.toml"))
}

// Model
#[tracker::track]
struct RawViewer {
    /// Num files
    num_files: usize,
    current_file: usize,
    current_file_name: Option<String>,
    /// Tell whether to show CFA or render.
    render: RenderingStage,
    #[tracker::do_not_track]
    /// The raw image.
    rawimage: Option<RawImage>,
    #[tracker::no_eq]
    /// Surface to draw
    surface: Option<cairo::ImageSurface>,
    toast_overlay: adw::ToastOverlay,
    width: u32,
    height: u32,
    /// Tell painting need to be done
    /// Reset at the begining of the update method.
    need_paint: bool,
    message: Option<String>,
    #[tracker::do_not_track]
    handler: drawing::DrawHandler,
    #[tracker::do_not_track]
    file_list: Controller<RawFileList>,
    #[tracker::do_not_track]
    open_dialog: Controller<relm4_components::open_dialog::OpenDialogMulti>,
    #[tracker::do_not_track]
    open_dir_dialog: Controller<relm4_components::open_dialog::OpenDialogMulti>,
    #[tracker::do_not_track]
    raw_info: Controller<RawInfo>,
}

#[derive(Debug)]
pub(crate) enum AppInput {
    /// When you really need to send a message.
    Ignore,
    Load(String, usize),
    Render(Box<libopenraw::Result<RawImage>>),
    ShowRawInfo(String),
    ShowThumbInfo(String),
    Redraw,
    Next,
    Previous,
    /// Ask to add file(s)
    AddFile,
    /// Ask to add dir(s)
    AddDir,
    /// Open the files. (from the open dialog)
    OpenFiles(Vec<std::path::PathBuf>),
    /// Open dirs. (from the open dialog)
    OpenDirs(Vec<std::path::PathBuf>),
    RemoveFile,
    SaveFiles,
    CountFiles(usize),
    ToggleRender(RenderingStage),
}

#[relm4::component]
impl SimpleComponent for RawViewer {
    type Init = Vec<String>;
    type Input = AppInput;
    type Output = ();
    type Widgets = RawViewerWidgets;

    fn init(
        files: Self::Init,
        root: Self::Root,
        sender: ComponentSender<Self>,
    ) -> ComponentParts<Self> {
        let open_dialog = OpenDialogMulti::builder()
            .transient_for_native(root.clone())
            .launch(OpenDialogSettings::default())
            .forward(sender.input_sender(), |response| match response {
                OpenDialogResponse::Accept(path) => AppInput::OpenFiles(path),
                _ => AppInput::Ignore,
            });
        let settings = OpenDialogSettings {
            folder_mode: true,
            ..Default::default()
        };
        let open_dir_dialog = OpenDialogMulti::builder()
            .transient_for_native(root.clone())
            .launch(settings)
            .forward(sender.input_sender(), |response| match response {
                OpenDialogResponse::Accept(path) => AppInput::OpenDirs(path),
                _ => AppInput::Ignore,
            });
        let raw_info = RawInfo::builder()
            .launch(())
            .forward(sender.input_sender(), identity);
        let file_list =
            RawFileList::builder()
                .launch(())
                .forward(sender.input_sender(), |response| match response {
                    RawFileListOutput::SelectionChanged(path, idx) => AppInput::Load(path, idx),
                    RawFileListOutput::CountChanged(c) => AppInput::CountFiles(c),
                });

        let model = RawViewer {
            num_files: 0,
            current_file: 0,
            current_file_name: None,
            render: RenderingStage::Raw,
            rawimage: None,
            surface: None,
            toast_overlay: adw::ToastOverlay::new(),
            width: 0,
            height: 0,
            need_paint: false,
            message: None,
            handler: DrawHandler::new(),
            file_list,
            open_dialog,
            open_dir_dialog,
            raw_info,
            tracker: 0,
        };
        let toast_overlay = &model.toast_overlay;
        let canvas = model.handler.drawing_area();
        let widgets = view_output!();

        // post_init
        if files.is_empty() {
            if let Some(config) = config_path() {
                println!("Loading config {config:?}");
                model.file_list.emit(RawFileListInput::LoadConfig(config));
            }
        } else {
            files.iter().cloned().for_each(|file| {
                model.file_list.emit(RawFileListInput::Add(file));
            });
            model.file_list.emit(RawFileListInput::Select(0));
        }

        ComponentParts { model, widgets }
    }

    view! {
        #[root]
        adw::ApplicationWindow {
            set_default_width: 800,
            set_default_height: 600,
            gtk::Box {
                set_orientation: gtk::Orientation::Vertical,
                adw::HeaderBar {
                    #[wrap(Some)]
                    set_title_widget = &adw::WindowTitle {
                        set_title: "libopenraw Viewer",
                        #[track = "model.changed(RawViewer::current_file_name())"]
                        set_subtitle: {
                            model.current_file_name
                                .as_deref()
                                .unwrap_or("No file")
                        }
                    }
                },
                gtk::Paned {
                    set_orientation: gtk::Orientation::Horizontal,
                    set_margin_all: 5,
                    #[wrap(Some)]
                    set_start_child = &gtk::Box {
                        set_orientation: gtk::Orientation::Vertical,
                        set_margin_all: 5,
                        set_spacing: 5,
                        set_vexpand: true,
                        gtk::Box {
                            set_orientation: gtk::Orientation::Horizontal,
                            add_css_class: "linked",
                            gtk::Button {
                                set_halign: gtk::Align::Start,
                                set_icon_name: "list-add-symbolic",
                                connect_clicked[sender] => move |_| {
                                    sender.input(AppInput::AddFile);
                                },
                            },
                            gtk::Button {
                                set_halign: gtk::Align::Start,
                                set_icon_name: "folder-new-symbolic",
                                connect_clicked[sender] => move |_| {
                                    sender.input(AppInput::AddDir);
                                },
                            },
                            gtk::Button {
                                set_halign: gtk::Align::Start,
                                set_icon_name: "document-save-symbolic",
                                connect_clicked[sender] => move |_| {
                                    sender.input(AppInput::SaveFiles);
                                },
                            },
                            gtk::Button {
                                set_halign: gtk::Align::End,
                                set_icon_name: "user-trash-symbolic",
                                add_css_class: "destructive-action",
                                connect_clicked[sender] => move |_| {
                                    sender.input(AppInput::RemoveFile);
                                },
                            },
                        },
                        append = model.file_list.widget(),
                    },
                    #[wrap(Some)]
                    set_end_child = &gtk::Box {
                        set_orientation: gtk::Orientation::Vertical,
                        set_margin_all: 5,
                        set_spacing: 5,
                        gtk::Box {
                            set_orientation: gtk::Orientation::Horizontal,
                            set_spacing: 5,
                            gtk::Box {
                                set_orientation: gtk::Orientation::Vertical,
                                set_spacing: 5,
                                set_hexpand: true,
                                gtk::Label {
                                    set_xalign: 0.0,
                                    set_use_markup: true,
                                    set_label: "<b>Raw file properties</b>",
                                },
                                append = model.raw_info.widget(),
                            },
                            gtk::Box {
                                set_orientation: gtk::Orientation::Vertical,
                                #[name(render_mode)]
                                gtk::CheckButton {
                                    set_label: Some("Raw"),
                                    set_active: model.render == RenderingStage::Raw,
                                    connect_active_notify[sender] => move |_| {
                                        sender.input(AppInput::ToggleRender(RenderingStage::Raw))
                                    }
                                },
                                gtk::CheckButton {
                                    set_label: Some("Linearize"),
                                    set_group: Some(&render_mode),
                                    set_active: model.render == RenderingStage::Linearization,
                                    connect_active_notify[sender] => move |_| {
                                        sender.input(AppInput::ToggleRender(RenderingStage::Linearization))
                                    }
                                },
                                gtk::CheckButton {
                                    set_label: Some("Interpolate"),
                                    set_group: Some(&render_mode),
                                    set_active: model.render == RenderingStage::Interpolation,
                                    connect_active_notify[sender] => move |_| {
                                        sender.input(AppInput::ToggleRender(RenderingStage::Interpolation))
                                    }
                                },
                                gtk::CheckButton {
                                    set_label: Some("RGB"),
                                    set_group: Some(&render_mode),
                                    set_active: model.render == RenderingStage::Colour,
                                    connect_active_notify[sender] => move |_| {
                                        sender.input(AppInput::ToggleRender(RenderingStage::Colour))
                                    }
                                }
                            }
                        },
                        #[local_ref]
                        toast_overlay -> adw::ToastOverlay {
                            #[track(skip_init, model.changed(RawViewer::message()) && model.message.is_some())]
                            add_toast: adw::Toast::new(model.message.as_ref().unwrap()),
                            gtk::ScrolledWindow {
                                set_hexpand: true,
                                set_vexpand: true,
                                #[local_ref]
                                canvas -> gtk::DrawingArea {
                                    #[watch]
                                    set_content_width: model.width as i32,
                                    #[watch]
                                    set_content_height: model.height as i32,
                                    connect_resize[sender] => move |_, _, _| {
                                        // All we need is to send a message to trigger
                                        // an update.
                                        // This is necessary as the first render of the
                                        // image is croped.
                                        sender.input(AppInput::Redraw);
                                    }
                                }
                            }
                        },
                        gtk::Box {
                            set_orientation: gtk::Orientation::Horizontal,
                            set_margin_all: 5,
                            set_spacing: 5,
                            set_homogeneous: true,
                            gtk::Button::with_label("Previous") {
                                #[track = "model.changed(RawViewer::current_file())"]
                                set_sensitive: model.current_file != 0,
                                connect_clicked[sender] => move |_| {
                                    sender.input(AppInput::Previous);
                                },
                            },
                            gtk::Button::with_label("Next") {
                                #[track = "model.changed(RawViewer::current_file()) || model.changed(RawViewer::num_files())"]
                                set_sensitive: model.current_file + 1 < model.num_files,
                                connect_clicked[sender] => move |_| {
                                    sender.input(AppInput::Next);
                                },
                            },
                        }
                    }
                }
            }
        }
    }

    fn update(&mut self, msg: Self::Input, sender: ComponentSender<Self>) {
        self.reset();
        self.need_paint = false;
        match msg {
            AppInput::Ignore => {}
            AppInput::CountFiles(c) => {
                self.set_num_files(c);
            }
            AppInput::SaveFiles => {
                if let Some(config) = config_path() {
                    println!("Save the file list");
                    self.file_list.emit(RawFileListInput::SaveTo(config));
                }
            }
            AppInput::Next => {
                self.file_list.emit(RawFileListInput::Next);
            }
            AppInput::Previous => {
                self.file_list.emit(RawFileListInput::Previous);
            }
            AppInput::AddFile => {
                self.open_dialog.emit(OpenDialogMsg::Open);
            }
            AppInput::AddDir => {
                self.open_dir_dialog.emit(OpenDialogMsg::Open);
            }
            AppInput::OpenFiles(list) => {
                let sender = self.file_list.sender().clone();
                list.iter().for_each(|p| {
                    if let Some(p) = p.as_os_str().to_str() {
                        sender.emit(RawFileListInput::Add(p.to_string()));
                    }
                });
            }
            AppInput::OpenDirs(list) => {
                let sender = self.file_list.sender().clone();
                list.iter()
                    .flat_map(|dir| files_from_directory(dir, true))
                    .for_each(|p| {
                        if let Some(p) = p.as_os_str().to_str() {
                            sender.emit(RawFileListInput::Add(p.to_string()));
                        }
                    });
            }
            AppInput::RemoveFile => {
                self.file_list.emit(RawFileListInput::RemoveSelected);
            }
            AppInput::Load(s, i) => {
                self.set_current_file_name(Some(s.clone()));
                self.set_current_file(i);
                self.set_surface(None);
                self.set_message(Some("Loading...".into()));
                self.rawimage = None;
                processing::process_rawfile(&s, sender);
            }
            AppInput::ShowRawInfo(s) => {
                self.raw_info.emit(RawInfoMsg::UpdateRawInfo(s));
            }
            AppInput::ShowThumbInfo(s) => {
                self.raw_info.emit(RawInfoMsg::UpdateThumbInfo(s));
            }
            AppInput::ToggleRender(render) => {
                self.render = render;
                if self.rawimage.is_some() {
                    self.set_message(Some("Rendering...".into()));
                    self.load_raw();
                }
            }
            AppInput::Render(rawdata) => {
                match *rawdata {
                    Ok(rawdata) => {
                        self.width = rawdata.width();
                        self.height = rawdata.height();
                        self.raw_info.emit(RawInfoMsg::UpdateRawDataInfo(
                            processing::raw_data_info(&rawdata),
                        ));
                        match rawdata.data_type() {
                            DataType::Raw => {
                                self.rawimage = Some(rawdata);
                                self.load_raw();
                            }
                            _ => {
                                self.rawimage = None;
                                self.set_surface(None);
                                self.set_message(Some(format!(
                                    "Not a recognized Raw format {:?}",
                                    rawdata.data_type()
                                )));
                            }
                        }
                    }
                    Err(_) => {
                        self.set_surface(None);
                        self.set_message(Some("Unknown Raw file".into()));
                        self.raw_info
                            .emit(RawInfoMsg::UpdateRawInfo(String::default()));
                        self.raw_info
                            .emit(RawInfoMsg::UpdateThumbInfo(String::default()));
                        self.raw_info
                            .emit(RawInfoMsg::UpdateRawDataInfo(String::default()));
                    }
                }
                self.need_paint = true;
            }
            AppInput::Redraw => {
                self.need_paint = true;
            }
        }

        // post_view
        let cx = self.handler.get_context();
        if self.need_paint || self.changed(RawViewer::surface()) {
            cx.set_source_rgba(1.0, 1.0, 1.0, 1.0);
            cx.paint().expect("Couldn't fill context");

            if let Some(ref surface) = self.surface {
                cx.set_source_surface(surface, 0.0, 0.0)
                    .expect("Couldn't set surface");
                cx.paint().expect("Couldn't paint");
            }
        }
    }
}

impl RawViewer {
    /// Load the Raw data into the surface
    fn load_raw(&mut self) {
        if self.rawimage.is_none() {
            self.set_surface(None);
            return;
        }
        let rawdata = self.rawimage.as_ref().unwrap();
        let options = RenderingOptions::default()
            .with_target(ColourSpace::SRgb)
            .with_stage(self.render);
        let image = rawdata.rendered_image(options);
        let surface = if self.render >= RenderingStage::Interpolation {
            image
                .and_then(|image| {
                    let height = image.height() as usize;
                    let width = image.width() as usize;
                    // XXX fix the values
                    match processing::image_to_surface(
                        width,
                        height,
                        image.whites()[0],
                        image.data16(),
                    ) {
                        Ok(surface) => {
                            self.set_message(None);
                            Ok(surface)
                        }
                        Err(err) => {
                            let msg: String = err.into();
                            self.set_message(Some(msg.clone()));
                            Err(libopenraw::Error::Other(msg))
                        }
                    }
                })
                .map_err(|err| {
                    self.set_message(Some(format!("Rendering error {err:?}")));
                    err
                })
                .ok()
        } else {
            let rawdata = image.as_ref().unwrap_or(rawdata);
            let height = rawdata.height() as usize;
            let width = rawdata.width() as usize;
            let mut white =
                std::cmp::min(rawdata.whites()[0] as u32, (1 << rawdata.bpc()) - 1) as u16;
            if white == 0 {
                println!("white is ZERO!");
                white = (1 << rawdata.bpc()) - 1;
            }
            println!("white {white}");
            match processing::raw_to_surface(width, height, white, rawdata.data16()) {
                Ok(surface) => {
                    self.set_message(None);
                    Some(surface)
                }
                Err(err) => {
                    self.set_message(Some(err.into()));
                    None
                }
            }
        };
        if let Some(ref surface) = surface {
            Self::paint_frames(surface, self.rawimage.as_ref().unwrap());
        }
        self.set_surface(surface);
    }

    fn paint_frames(surface: &cairo::Surface, image: &RawImage) {
        let context = cairo::Context::new(surface).unwrap();
        if let Some(active_area) = image.active_area() {
            context.set_source_rgb(0.0, 1.0, 0.0);
            context.rectangle(
                active_area.x as f64,
                active_area.y as f64,
                active_area.width as f64,
                active_area.height as f64,
            );
            let _ = context.stroke();
        }
        if let Some(user_crop) = image.user_crop() {
            context.set_source_rgb(1.0, 0.0, 0.0);
            context.rectangle(
                user_crop.x as f64,
                user_crop.y as f64,
                user_crop.width as f64,
                user_crop.height as f64,
            );
            let _ = context.stroke();
        }
    }
}

fn main() {
    let files = std::env::args().skip(1).collect();
    let app = RelmApp::new("org.freedesktop.libopenraw.Viewer");
    app.run::<RawViewer>(files);
}
