// SPDX-License-Identifier: GPL-3.0-or-later

use gtk4::prelude::*;
use relm4::gtk as gtk4;
use relm4::{ComponentParts, ComponentSender, SimpleComponent};

use crate::AppInput;

#[derive(Debug)]
#[allow(clippy::enum_variant_names)]
pub(crate) enum RawInfoMsg {
    UpdateRawInfo(String),
    UpdateThumbInfo(String),
    UpdateRawDataInfo(String),
}

#[derive(Default)]
pub(crate) struct RawInfo {
    raw_info: String,
    raw_thumb_info: String,
    raw_data_info: String,
}

#[relm4::component(pub(crate))]
impl SimpleComponent for RawInfo {
    type Init = ();
    type Input = RawInfoMsg;
    type Output = AppInput;

    fn init(
        _params: Self::Init,
        root: Self::Root,
        _sender: ComponentSender<Self>,
    ) -> ComponentParts<Self> {
        let model = RawInfo::default();
        let widgets = view_output!();

        ComponentParts { model, widgets }
    }

    fn update(&mut self, msg: Self::Input, _sender: ComponentSender<Self>) {
        use RawInfoMsg::*;

        match msg {
            UpdateRawInfo(s) => self.raw_info = s,
            UpdateThumbInfo(s) => self.raw_thumb_info = s,
            UpdateRawDataInfo(s) => self.raw_data_info = s,
        }
    }

    view! {
        #[root]
        gtk4::Box {
            set_orientation: gtk4::Orientation::Horizontal,
            set_homogeneous: true,
            set_spacing: 5,
            gtk4::Frame {
                set_label: Some("Raw file"),
                gtk4::Box {
                    set_orientation: gtk4::Orientation::Vertical,
                    set_spacing: 5,
                    gtk4::Label {
                        set_xalign: 0.0,
                        set_use_markup: true,
                        set_wrap: true,
                        #[watch]
                        set_label: &model.raw_info,
                    }
                }
            },
            gtk4::Frame {
                set_label: Some("Thumbnails"),
                gtk4::ScrolledWindow {
                    set_valign: gtk4::Align::Fill,
                    gtk4::Label {
                        set_xalign: 0.0,
                        set_use_markup: true,
                        set_wrap: true,
                        #[watch]
                        set_label: &model.raw_thumb_info,
                    }
                }
            },
            gtk4::Frame {
                set_label: Some("Raw data"),
                gtk4::Box {
                    set_orientation: gtk4::Orientation::Vertical,
                    set_spacing: 5,
                    gtk4::Label {
                        set_xalign: 0.0,
                        set_use_markup: true,
                        set_wrap: true,
                        #[watch]
                        set_label: &model.raw_data_info,
                    }
                }
            }
        }
    }
}
