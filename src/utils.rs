// SPDX-License-Identifier: GPL-3.0-or-later

use walkdir::WalkDir;

pub fn files_from_directory<P>(dir: P, recursive: bool) -> Vec<std::path::PathBuf>
where
    P: AsRef<std::path::Path>,
{
    if !dir.as_ref().is_dir() {
        println!("Not a directory: {:?}", dir.as_ref());
        return vec![];
    }

    if recursive {
        WalkDir::new(&dir).follow_links(true)
    } else {
        WalkDir::new(&dir).max_depth(1).follow_links(true)
    }
    .into_iter()
    // ignore everything that starts with a '.'
    .filter_entry(|entry| {
        !entry
            .file_name()
            .to_str()
            .map(|s| s.starts_with('.'))
            .unwrap_or(false)
    })
    .flatten()
    .filter_map(|entry| {
        let ftype = entry.file_type();
        if ftype.is_file() || ftype.is_symlink() {
            Some(entry.path().to_path_buf())
        } else {
            None
        }
    })
    .collect::<Vec<std::path::PathBuf>>()
}
