// SPDX-License-Identifier: GPL-3.0-or-later

use libopenraw::{metadata::Value, Bitmap, RawFile, RawImage};
use relm4::gtk::cairo;
use relm4::ComponentSender;

use super::{AppInput, RawViewer};

pub(super) fn raw_data_info(rawdata: &RawImage) -> String {
    let type_ = rawdata.data_type();
    let w = rawdata.width();
    let h = rawdata.height();
    let mut output = format!(
        "Format: {type_:?}\n\
         Dimensions: {w}x{h}"
    );
    if let Some(active_area) = rawdata.active_area() {
        output.push_str(&format!("\nActive area: {active_area:?}"));
    }
    if let Some(user_crop) = rawdata.user_crop() {
        output.push_str(&format!("\nUser crop: {user_crop:?}"));
    }
    if let Some(user_aspect_ratio) = rawdata.user_aspect_ratio() {
        output.push_str(&format!("\nAspect ratio: {user_aspect_ratio}"));
    }
    let pattern = rawdata.mosaic_pattern().to_string();
    let bpc = rawdata.bpc();
    let whites = rawdata.whites();
    let blacks = rawdata.blacks();
    let as_shot = rawdata.as_shot_neutral();
    output.push_str(&format!(
        "\nPattern: {pattern}\n\
         Bit per component: {bpc}\n\
         Black values: {blacks:?}\n\
         White values: {whites:?}\n\
         White Balance: {as_shot:?}"
    ));
    let has_linearization_table = rawdata.linearization_table().is_some();
    if has_linearization_table {
        output.push_str("\nHas linearization table.");
    }
    output
}

pub(super) fn rawfile_info(rawfile: &dyn RawFile) -> String {
    let type_ = rawfile.type_();
    let type_id = rawfile.type_id();
    let orientation = rawfile.orientation();
    let mut output = format!(
        "Type: {type_:?}\n\
         ID: {type_id}\n\
         Orientation: {orientation}"
    );
    if let Some(make) = rawfile
        .metadata_value("Exif.Image.Make")
        .as_ref()
        .and_then(Value::string)
    {
        output.push_str(&format!("\nMake: {make}"));
    }
    if let Some(model) = rawfile
        .metadata_value("Exif.Image.Model")
        .as_ref()
        .and_then(Value::string)
    {
        output.push_str(&format!("\nModel: {model}"));
    }
    if let Some(model) = rawfile
        .metadata_value("Exif.Image.UniqueCameraModel")
        .as_ref()
        .and_then(Value::string)
    {
        output.push_str(&format!("\nUnique Model: {model}"));
    }
    for i in 1..=2 {
        let illuminant = rawfile.calibration_illuminant(i);
        if illuminant as u32 != 0 {
            let illuminant_n = illuminant as u32;
            let matrix_origin = rawfile
                .colour_matrix(i)
                .map(|matrix| matrix.0)
                .unwrap_or_default();
            output.push_str(&format!(
                "\nCalibration illuminant {i}: {illuminant_n} ({illuminant:?})\n\
                 Matrix: {matrix_origin:?}"
            ));
        }
    }

    output
}

pub(super) fn thumbnails_info(rawfile: &dyn RawFile) -> String {
    let sizes = rawfile.thumbnail_sizes();
    let mut output = format!("Thumbnail sizes: {:?}", sizes);
    for (idx, size) in sizes.iter().enumerate() {
        let thumbnail = rawfile.thumbnail(*size);
        if let Ok(thumbnail) = thumbnail {
            let w = thumbnail.width();
            let h = thumbnail.height();
            let type_ = thumbnail.data_type();
            let data_size = thumbnail.data_size();
            output.push_str(&format!(
                "\n#{idx}\n\
                 Size: {w}x{h}\n\
                 Type: {type_:?}\n\
                 Bytes: {data_size}\
                 "
            ));
        } else {
            output.push_str(&format!("\n#{idx}, size {size} not found."));
        }
    }
    output
}

pub(super) fn process_rawfile(filename: &str, sender: ComponentSender<RawViewer>) {
    let filename = filename.to_string();
    println!("Reading file {}", &filename);
    std::thread::spawn(move || {
        sender.input(AppInput::Render(
            Box::new(libopenraw::rawfile_from_file(&filename, None)
                .map_err(|err| {
                    println!("Error reading file {}: {}", &filename, err);
                    err
                })
                .and_then(|rawfile| {
                    println!("Read Raw data from file {}", &filename);
                    sender.input(AppInput::ShowRawInfo(rawfile_info(&*rawfile)));
                    sender.input(AppInput::ShowThumbInfo(thumbnails_info(&*rawfile)));
                    rawfile.raw_data(false)
                })
                .map_err(|err| {
                    println!("Error raw data {}: {}", &filename, err);
                    err
                }),
        )));
    });
}

pub(super) fn raw_to_surface(
    width: usize,
    height: usize,
    white: u16,
    data16: Option<&[u16]>,
) -> Result<cairo::ImageSurface, &'static str> {
    data16.ok_or("No data").and_then(|data16| {
        let stride = cairo::Format::A8
            .stride_for_width(width as u32)
            .unwrap_or(width as i32);
        let mut data = vec![0_u8; height * stride as usize];
        let scale = 255.0 / white as f64;
        for y in 0..height {
            let pos = y * width;
            let dest_pos = y * stride as usize;
            if pos + width > data16.len() {
                return Err("Error in Raw data");
            }
            data.as_mut_slice()[dest_pos..dest_pos + width].copy_from_slice(
                &data16[pos..pos + width]
                    .iter()
                    .map(|v| (*v as f64 * scale) as u8)
                    .collect::<Vec<u8>>(),
            );
        }

        cairo::ImageSurface::create_for_data(
            data,
            cairo::Format::A8,
            width as i32,
            height as i32,
            stride,
        )
        .map_err(|_| "Cairo error")
    })
}

pub(super) fn image_to_surface(
    width: usize,
    height: usize,
    white: u16,
    data16: Option<&[u16]>,
) -> Result<cairo::ImageSurface, &'static str> {
    data16.ok_or("No data").and_then(|data16| {
        let stride = cairo::Format::Rgb24
            .stride_for_width(width as u32)
            .unwrap_or(width as i32);
        let mut data = vec![0_u8; height * (stride as usize)];
        let mut row = vec![0_u8; stride as usize];
        let scale = 255.0 / white as f64;
        for y in 0..height {
            // pixel width in the source (RGB)
            let px_w = width * 3;
            let pos = y * px_w;
            let dest_pos = y * stride as usize;
            if pos + px_w > data16.len() {
                println!(
                    "Short read of Image data: h: {}, {} + {} (= {})> {}",
                    y,
                    pos,
                    px_w,
                    pos + px_w,
                    data16.len()
                );
                return Err("Error in Image data");
            }
            let mut dest = 0;
            // RGB to 0RGB for the Rgb24 Cairo format.
            let mut i = pos;
            while i < pos + px_w {
                let r = (data16[i] as f64 * scale) as u32;
                i += 1;
                let g = (data16[i] as f64 * scale) as u32;
                i += 1;
                let b = (data16[i] as f64 * scale) as u32;
                i += 1;
                let rgb = (r << 16) + (g << 8) + b;
                row[dest..dest + 4].copy_from_slice(&rgb.to_ne_bytes());
                dest += 4;
            }
            data.as_mut_slice()[dest_pos..dest_pos + (stride as usize)].copy_from_slice(&row);
        }

        cairo::ImageSurface::create_for_data(
            data,
            cairo::Format::Rgb24,
            width as i32,
            height as i32,
            stride,
        )
        .map_err(|_| "Cairo Error")
    })
}
